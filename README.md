　　﻿作者：qlslylq (浅蓝深蓝叶落秋)

　　联系：13297089301 QQ：2939143482

　　QQ讨论群：326550832(AndroidDesign)

　　名下开源框架：

　　1.AndroidDesign(eclipse，in 2014)

　　2.AndroidDesign(AndroidStudio，in 2016)

　　3.AdmxSDK及AdmxSDK_v2(AndroidStudio，in 2017)

　　4.UnityDesign(VisualStudio,C#，in 2017)

　　5.FlutterDesign(AndroidStudio，in 2019)

　　名下开源项目：

　　1.AndroidDesignQtfy(且听风吟app by eclipse;downloadApp：http://qlslylq.bmob.site)

　　2.UnityDesignEasyAR (AR纹理识别demo)

　　3.UnityDesignThreeInOne (三合一游戏demo)

# FlutterDesign

#### 介绍
Flutter跨平台综合快速开发框架

#### 软件架构
使用传统的MV架构搭建窗口层及网络层。简单而易用，直接而快速。


#### 安装教程

1. using plugins with Dart sdk,Flutter sdk
2. using development tools with AndroidStudio or VisualStudio
3. using network library with dio

#### 使用说明

1. 窗口层，网络层的使用风格沿用名下AndroidDesign(AndroidStudio)框架风格
2. 由于所使用的应用窝后端云服务器关闭，影响了开发计划，诸多架构细节及组件库等后期逐渐调整
3. 由于计划被打乱及时间紧迫，不足之处及符合Flutter的生态各模块请各位自行增加，调整及优化！

#### 框架截图

![窗口层使用](https://gitee.com/uploads/images/2019/0406/210233_46ec8773_452019.png "Screenshot_2019-04-06-20-40-11-370_com.qlslylq.fl.png")

![窗口层使用](https://gitee.com/uploads/images/2019/0406/210315_0753d862_452019.png "Screenshot_2019-04-06-20-40-17-691_com.qlslylq.fl.png")

![网络层使用](https://gitee.com/uploads/images/2019/0406/224852_a621f8fc_452019.png "Screenshot_2019-04-06-22-47-21-255_com.qlslylq.fl.png")