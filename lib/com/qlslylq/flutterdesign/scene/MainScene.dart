import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/scene/BaseScene.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/scene/PostingScene.dart';

void main() {
  runApp(new MainScene());
}

/*
 * 主页(框架用例) <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class MainScene extends BaseScene {

  State<StatefulWidget> createState() {
    return new MainSceneState();
  }

}

/*
 * 页面功能 <br/>
 */
class MainSceneState extends BaseSceneState {

  MainSceneState() {
    title = 'FlutterDesign';
    setRightButtonFromIcon(Icons.menu);
    setBorderButtonFromIcon(Icons.add);
    leading = new Container(width: 0, height: 0);
  }

  Widget build(BuildContext context) {
    var listTileHeight = 33.5;
    var listTileMargin = EdgeInsets.fromLTRB(3, 3, 3, 1);
    var listTileMarginBottom = EdgeInsets.fromLTRB(3, 3, 3, 10);

    return buildBody(context,
        new ListView(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
            children: <Widget>[

              new Container(
                height: listTileHeight,
                margin: listTileMarginBottom,
                child: new RaisedButton(onPressed: () {
                  showPostingScene();
                },
                  child: new Text('请求网络数据'),
                  color: Theme
                      .of(context)
                      .buttonColor,
                  shape: Border.all(color: Colors.red,
                      style: BorderStyle.solid,
                      width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    title = 'FlutterDesign~';
                  });
                }, child: new Text('设置标题'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.green,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMarginBottom,
                child: new RaisedButton(
                  onPressed: () {
                    setState(() {
                      titleBarBg = Colors.green;
                    });
                  }, child: new Text('设置标题栏背景'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.green,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setRightButtonFromAsset(
                        'assets/images/app_icon_default.png');
                  });
                }, child: new Text('设置右边按钮(FromAsset)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.blue,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setRightButtonFromIcon(Icons.menu);
                  });
                }, child: new Text('设置右边按钮(FromIcon)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.blue,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setRightButtonFromText('右边');
                  });
                }, child: new Text('设置右边按钮(FromText)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.blue,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMarginBottom,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    btn_right = null;
                  });
                }, child: new Text('隐藏右边按钮'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.blue,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setBorderButtonFromAsset(
                        'assets/images/app_icon_default.png');
                  });
                }, child: new Text('设置侧边按钮(FromAsset)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.yellow,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setBorderButtonFromIcon(Icons.add);
                  });
                }, child: new Text('设置侧边按钮(FromIcon)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.yellow,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setBorderButtonFromText('侧边');
                  });
                }, child: new Text('设置侧边按钮(FromText)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.yellow,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMarginBottom,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    btn_border = null;
                  });
                }, child: new Text('隐藏侧边按钮'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.yellow,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setLeadingButtonFromAsset(
                        'assets/images/app_icon_default.png');
                  });
                }, child: new Text('设置左边按钮(FromAsset)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.teal,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    setLeadingButtonFromIcon(Icons.search);
                  });
                }, child: new Text('设置左边按钮(FromIcon)'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.teal,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMarginBottom,
                child: new RaisedButton(onPressed: () {
                  setState(() {
                    leading = new Container(width: 0, height: 0);
                  });
                }, child: new Text('隐藏左边按钮'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.teal,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(
                  onPressed: () {
                    showToast('Hello,FlutterDesign~');
                  }, child: new Text('showToast'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.cyan,
                    style: BorderStyle.solid,
                    width: 1),),),

              new Container(
                height: listTileHeight,
                margin: listTileMargin,
                child: new RaisedButton(
                  onPressed: () {
                    showSnackBar('Hello,FlutterDesign~', label: 'continue');
                  }, child: new Text('showSnackBar'), color: Theme
                    .of(context)
                    .buttonColor, shape: Border.all(color: Colors.cyan,
                    style: BorderStyle.solid,
                    width: 1),),),

            ]));
  }

  void onClick(Widget widget) {
    if (widget.key == key_btn_right) {
      showSnackBar('点击了右边按钮~');
      showPostingScene();
    } else if (widget.key == key_btn_border) {
      showSnackBar('点击了侧边按钮~');
      showPostingScene();
    } else if (widget.key == key_btn_leading) {
      showSnackBar('点击了左边按钮~');
    }
  }

  /*
   * 跳转帖子页
   */
  void showPostingScene() {
    switchingScene(new PostingScene());
  }

}