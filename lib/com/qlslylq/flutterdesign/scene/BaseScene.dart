import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/http/listener/HttpListener.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/Log.dart';
import 'package:fluttertoast/fluttertoast.dart';

/*
 * Scene基类<br/>
 */
abstract class BaseScene extends StatefulWidget {}

/*
 * SceneState基类 <br/>
 * 功能封装：标题栏函数化；正向路由与逆向路由 <br/>
 */
abstract class BaseSceneState extends State<BaseScene> implements HttpListener {

  //右边按钮标识
  final key_btn_right = GlobalKey();

  //侧边按钮标识
  final key_btn_border = GlobalKey();

  //左边按钮标识
  final key_btn_leading = GlobalKey();

  //窗口
  MaterialApp app;

  //body上下文
  BuildContext bodyContext;

  //标题
  String title;

  //右边按钮与侧边按钮
  IconButton btn_right, btn_border;

  //leading
  Widget leading;

  //标题栏背景
  Color titleBarBg;

  /*
   * 创建内容栏
   */
  Widget buildBody(BuildContext context, Widget body) {
    app = new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          leading: leading ?? new IconButton(key: key_btn_leading,
            icon: new Icon(Icons.arrow_back), onPressed: () {
              _onClickLeadingButton(leading);
            },),
          title: new Text(title ?? 'FlutterDesign'),
          centerTitle: true,
          actions: <Widget>[
            btn_border ?? new Container(width: 0, height: 0),
            btn_right ?? new Container(width: 0, height: 0),
          ],
          backgroundColor: titleBarBg ?? Color.fromARGB(127, 127, 127, 127),
        ),
        body:
        new Builder(builder: (BuildContext context) {
          this.bodyContext = context;
          return body;
        }),
        backgroundColor: Colors.white,
      )
      ,
    );
    return app;
  }

  /*
   * 设置右边按钮
   */
  void setRightButtonFromAsset(String path) {
    btn_right = new IconButton(key: key_btn_right,
      icon: new Image.asset(path), onPressed: () {
        onClick(btn_right);
      },);
  }

  /*
   * 设置右边按钮
   */
  void setRightButtonFromIcon(IconData iconData) {
    btn_right = new IconButton(key: key_btn_right,
      icon: new Icon(iconData), onPressed: () {
        onClick(btn_right);
      },);
  }

  /*
   * 设置右边按钮
   */
  void setRightButtonFromText(String text) {
    btn_right = new IconButton(key: key_btn_right,
      icon: new Text(text), onPressed: () {
        onClick(btn_right);
      },);
  }

  /*
   * 设置侧边按钮
   */
  void setBorderButtonFromAsset(String path) {
    btn_border = new IconButton(key: key_btn_border,
      icon: new Image.asset(path), onPressed: () {
        onClick(btn_border);
      },);
  }

  /*
   * 设置侧边按钮
   */
  void setBorderButtonFromIcon(IconData iconData) {
    btn_border = new IconButton(key: key_btn_border,
      icon: new Icon(iconData), onPressed: () {
        onClick(btn_border);
      },);
  }

  /*
   * 设置侧边按钮
   */
  void setBorderButtonFromText(String text) {
    btn_border = new IconButton(key: key_btn_border,
      icon: new Text(text), onPressed: () {
        onClick(btn_border);
      },);
  }

  /*
   * 设置左边按钮
   */
  void setLeadingButtonFromAsset(String path, {bool selfHandle = true}) {
    leading = new IconButton(key: key_btn_leading,
      icon: new Image.asset(path), onPressed: () {
        if (selfHandle) {
          onClick(leading);
        } else {
          _onClickLeadingButton(leading);
        }
      },);
  }

  /*
   * 设置左边按钮
   */
  void setLeadingButtonFromIcon(IconData iconData, {bool selfHandle = true}) {
    leading = new IconButton(key: key_btn_leading,
      icon: new Icon(iconData), onPressed: () {
        if (selfHandle) {
          onClick(leading);
        } else {
          _onClickLeadingButton(leading);
        }
      },);
  }

  /*
   * 控件点击
   */
  void onClick(Widget widget) {
    showSnackBar('正在开发中');
  }

  /*
   * 返回按钮点击
   */
  void _onClickLeadingButton(Widget widget) {
    Navigator.pop(context);
  }

  /*
   * 显示Toast
   */
  void showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      backgroundColor: Color.fromARGB(127, 127, 127, 127),
      textColor: Colors.white,
    );
  }

  /*
   * 显示SnackBar
   */
  void showSnackBar(String msg, {String label = '确定'}) {
    final snackBar = new SnackBar(
      content: new Text(msg),
      action: new SnackBarAction(label: label, onPressed: onSnackBarPressed),
    );
    Scaffold.of(bodyContext).showSnackBar(snackBar);
  }

  /*
   * SnackBarAction点击回调
   */
  void onSnackBarPressed() {

  }

  /*
   * 设置进度栏文本
   */
  void setProgressBarText(String text) {

  }

  /*
   * 重置进度栏文本
   */
  void resetProgressBarText() {

  }

  /*
   * 显示进度条及隐藏输入法<br/>
   */
  void showProgressBar() {

  }

  /*
   * 隐藏进度条<br/>
   */
  void hideProgressBar() {

  }

  /*
   * 隐藏输入法<br/>
   */
  void hideSoftInput() {

  }

  /*
   * 打开指定场景
   */
  void switchingScene(BaseScene scene) {
    Navigator.push(bodyContext, new MaterialPageRoute(builder: (context) {
      return scene;
    },));
  }

  /*
   * 返回上一场景
   */
  void finish() {
    Navigator.pop(context);
  }

  void onDbSucceed(String method, Object values) {
    showToast("" + values);
  }

  void onDbFaild(String method, Object values) {
    showToast(values);
    Log.e("db", values);
  }

  void onNetWorkSucceed(String method, Object values) {
    showToast("" + values);
  }

  void onNetWorkFaild(String method, Object values) {
    showToast("" + values);
    Log.e("http", values);
  }

  void onOtherSucceed(String method, Object values) {
    showToast("" + values);
  }

  void onOtherFaild(String method, Object values) {
    showToast("" + values);
    Log.e("other", values);
  }

  void onException(String method, Object e) {
    String error = "网络连接出现异常："
        + "\n    异常运行窗口：" + runtimeType.toString()
        + "\n    捕获异常函数：" + method
        + "\n    " + e.toString();
    Log.e(Log.TAG, error);
    Log.e("error", error);
    showToast(error);
  }

  void onCancel(String method) {

  }

}
