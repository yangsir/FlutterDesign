import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Parcel.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/http/service/PostingService.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/scene/BaseScene.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/Log.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/widget/ListViewWidget.dart';

void main() {
  runApp(new PostingScene());
}

/*
 * 帖子列表页 <br/>
 * 需要传入的键：<br/>
 * 传入的值类型： <br/>
 * 传入的值含义：<br/>
 * 是否必传 ：
 */
class PostingScene extends BaseScene {

  State<StatefulWidget> createState() {
    return new PostingSceneState();
  }

}

/*
 * 页面功能 <br/>
 */
class PostingSceneState extends BaseSceneState {

  Widget widget_body=new Center(
      child:new Text('正在加载数据...')
  );

  PostingSceneState() {
    title = '快递查询';
    setRightButtonFromIcon(Icons.add);
  }

  void initState() {
    super.initState();
    query(1);
  }

  Widget build(BuildContext context) {
    Widget widget = buildBody(context, widget_body);
    return widget;
  }

  void onClick(Widget widget) {
    if (widget.key == key_btn_right) {
      showSnackBar('发布帖子正在开发中~');
    }
  }

  void query(int page) {
    PostingService.queryTest(null, page, this);
  }

  void onNetWorkFaild(String method, Object values) {
  }

  void onNetWorkSucceed(String method, Object values) {
    if (method == "queryPostingList") {

    } else if (method == "queryTest") {
      setState(() {
        if(values is List<Parcel>) {
          widget_body = new ListViewWidget(list: values);
        }else{
          widget_body = new Center(
              child:new Text('查无结果')
          );
        }});
    }
  }

}

