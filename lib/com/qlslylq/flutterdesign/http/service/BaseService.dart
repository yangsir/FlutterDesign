import 'package:flutter_design/com/qlslylq/flutterdesign/constant/MessageConstant.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/constant/WhatConstant.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/http/listener/HttpListener.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/scene/BaseScene.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/TextUtils.dart';

/*
 * 接口服务基类：数据的聚合与分发<br/>
 * 网络返回后，数据分发到窗口层<br/>
 * 网络连接开始后，自动执行进度条的显示<br/>
 * 网络连接回调后，自动执行进度条的隐藏<br/>
 */
class BaseService {

  /*
   * 分发消息到窗口层<br/>
   * 自动控制ProgressBar的显示与隐藏<br/>
   */
  static void sendMessage(final String method, final dynamic data,
      final int what, final HttpListener listener) {
    showProgressBar(listener);
    if (data != MessageConstant.MSG_EMPTY) {
      switch (what) {
        case WhatConstant.WHAT_NET_DATA_SUCCESS:
          listener.onNetWorkSucceed(method, data);
          break;
        case WhatConstant.WHAT_NET_DATA_FAIL:
          listener.onNetWorkFaild(method, data);
          break;
        case WhatConstant.WHAT_DB_DATA_SUCCESS:
          listener.onDbSucceed(method, data);
          break;
        case WhatConstant.WHAT_DB_DATA_FAIL:
          listener.onDbFaild(method, data);
          break;
        case WhatConstant.WHAT_OTHER_DATA_SUCCESS:
          listener.onOtherSucceed(method, data);
          break;
        case WhatConstant.WHAT_OTHER_DATA_FAIL:
          listener.onOtherFaild(method, data);
          break;
        case WhatConstant.WHAT_EXCEPITON:
          listener.onException(method, data);
          break;
        case WhatConstant.WHAT_CANCEL:
          listener.onCancel(method);
          break;
      }
    }
    hideProgressBar(listener);
  }

  /*
   * 获取窗口
   */
  static BaseSceneState getBaseSceneState(HttpListener listener) {
    if (listener is BaseSceneState) {
      return listener as BaseSceneState;
    }
    return null;
  }

  /*
   * 设置网络进度栏文本
   */
  static void setProgressBarText(HttpListener listener, String text) {
    getBaseSceneState(listener).setProgressBarText(text);
  }

  /*
   * 显示网络进度栏
   */
  static void showProgressBar(HttpListener listener) {
    getBaseSceneState(listener).showProgressBar();
  }

  /*
   * 隐藏并重置网络进度栏
   */
  static void hideProgressBar(HttpListener listener) {
    BaseSceneState activity = getBaseSceneState(listener);
    activity.hideProgressBar();
    activity.resetProgressBarText();
  }

  /*
   * 显示Toast<br/>
   * 异步调用<br/>
   */
  static void showToast(HttpListener listener, final String text) {
    final BaseSceneState activity = getBaseSceneState(listener);
    activity.showToast(text);
  }

  /*
   * 判断网络返回的JsonObject数据是否无效<br/>
   * 并自动进行失败处理<br/>
   */
  static bool isDataInvalid(final String method, Map<String, dynamic> jo,
      HttpListener listener) {
    if (jo == null) {
      sendMessage(method,
          MessageConstant.MSG_CLIENT_FAILED, WhatConstant.WHAT_NET_DATA_FAIL,
          listener);
      return true;
    }
    //106错误，待后台纠正后改进此判断(暂删除102与303判断，以实际接口返回数据为准)
    else if ("106" == jo["code"]) {
      sendMessage(method,
          MessageConstant.MSG_LOGIN_INVALID_AUTO,
          WhatConstant.WHAT_NET_DATA_FAIL, listener
      );
      //autoLogin
      return true;
    } else if ("-1" == (jo["code"])) {
      String error = jo["error"];
      sendMessage(method,
          TextUtils.isEmpty(error) ? MessageConstant.MSG_SERVER_BUSY : error,
          WhatConstant.WHAT_NET_DATA_FAIL, listener
      );
      return true;
    } else if ("304" == jo["code"]) {
      sendMessage(method, MessageConstant.MSG_NEED_LOGIN,
          WhatConstant.WHAT_NET_DATA_FAIL, listener);
      //switchingSceneToLoginScene
      return true;
    } else if ("0" != jo["code"]) {
      sendMessage(
          method, jo["error"], WhatConstant.WHAT_NET_DATA_FAIL, listener);
      return true;
    }
    return false;
  }

}