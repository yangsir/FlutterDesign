import 'package:dio/dio.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Pager.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Parcel.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Posting.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/User.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/constant/MessageConstant.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/constant/WhatConstant.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/http/engine/HttpProtocol.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/http/listener/HttpListener.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/http/service/BaseService.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/util/TextUtils.dart';
import 'dart:convert';

/*
 * 帖子模块 <br/>
 */
class PostingService extends BaseService {

  /*
   * 查询帖子<br/>
   */
  static void queryPostingList(final String tag, final int page,
      final HttpListener listener) {
    BaseService.setProgressBarText(listener, "正在查询帖子");

    HttpProtocol protocol = new HttpProtocol();
    protocol.setService("posting").setMethod("query")
        .addParam("tag", TextUtils.isEmpty(tag) ? "" : tag)
        .addPagerParam(page)
        .post().then((response) {
      if (BaseService.isDataInvalid(
          "queryPostingList", response.data, listener)) {
        return MessageConstant.MSG_EMPTY;
      }

      Map<String, dynamic> data = response.data["body"];

      Map<String, Object> map = new Map<String, Object>();

      Pager pager = json.decode(data.toString());
      List<Posting> list = json.decode(data["list"]);
      List<User> users = json.decode(data[list]);

      for (int index = 0; index < list.length; index++) {
        list[index].setUser(users[index]);
      }

      map["pager"] = pager;
      map["list"] = list;

      print('pager：$pager');
      print('list：${list}');
      print('list：${list.length}');
      return data;
    }).then((data) {
      BaseService.sendMessage("queryPostingList", data,
          WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e) {
      BaseService.sendMessage("queryPostingList", e,
          WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

  /*
   * 查询<br/>
   */
  static void queryTest(final String tag, final int page,
      final HttpListener listener) {
    BaseService.setProgressBarText(listener, "正在查询帖子");

    HttpProtocol protocol = new HttpProtocol();
    protocol.url = "http://www.kuaidi100.com/query";
    protocol.addParam("type", "shentong")
        .addParam("postid", "100000")
        .post()
        .then((response) {
          if(response.data["data"]!=null) {
            dynamic dataJson = response.data["data"] ;
            List<Parcel> data = new List<Parcel>();
            dataJson.forEach((item) => data.add(new Parcel.fromJson(item)));
            return data;
          }else{
            return MessageConstant.MSG_EMPTY;
          }
    }).then((data) {
      BaseService.sendMessage("queryTest", data,
          WhatConstant.WHAT_NET_DATA_SUCCESS, listener);
    }).catchError((e) {
      BaseService.sendMessage("queryTest", e,
          WhatConstant.WHAT_EXCEPITON, listener);
    });
  }

}