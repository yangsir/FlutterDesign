// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Pager.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pager _$PagerFromJson(Map<String, dynamic> json) {
  return Pager()
    ..totalCount = json['totalCount'] as int
    ..pageSize = json['pageSize'] as int
    ..pageNo = json['pageNo'] as int
    ..totalPage = json['totalPage'] as int
    ..currentPage = json['currentPage'] as int
    ..perPageRows = json['perPageRows'] as int;
}

Map<String, dynamic> _$PagerToJson(Pager instance) => <String, dynamic>{
      'totalCount': instance.totalCount,
      'pageSize': instance.pageSize,
      'pageNo': instance.pageNo,
      'totalPage': instance.totalPage,
      'currentPage': instance.currentPage,
      'perPageRows': instance.perPageRows
    };
