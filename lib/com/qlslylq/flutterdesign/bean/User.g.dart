// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'User.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..account = json['account'] as String
    ..password = json['password'] as String
    ..id = json['主键'] as String
    ..uid = json['uid'] as String
    ..username = json['账号'] as String
    ..avatars = (json['avatars'] as List)?.map((e) => e as String)?.toList()
    ..avatar = json['avatar'] as String
    ..role = json['角色'] as String
    ..sex = json['性别'] as String
    ..birthday = json['出生日期'] as String
    ..mobile = json['手机号'] as String
    ..createtime = json['创建时间'] as String
    ..updatetime = json['更新时间'] as String
    ..creater = json['创建人'] as String;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'account': instance.account,
      'password': instance.password,
      '主键': instance.id,
      'uid': instance.uid,
      '账号': instance.username,
      'avatars': instance.avatars,
      'avatar': instance.avatar,
      '角色': instance.role,
      '性别': instance.sex,
      '出生日期': instance.birthday,
      '手机号': instance.mobile,
      '创建时间': instance.createtime,
      '更新时间': instance.updatetime,
      '创建人': instance.creater
    };
