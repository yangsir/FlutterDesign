import 'package:flutter/material.dart';
import 'package:flutter_design/com/qlslylq/flutterdesign/bean/Parcel.dart';

class ListViewWidget extends StatefulWidget {

  final List<Parcel> list;

  const ListViewWidget({Key key, @required this.list}) : super(key: key);

  State<StatefulWidget> createState() {
    return new ListViewState();
  }
}

class ListViewState extends State<ListViewWidget> {

  Widget build(BuildContext context) {
    List<ListTile> list_tile = widget.list.map((item) {
      return new ListTile(
        title: new Text('${item.context}'),
      );
    }).toList();

    List<Widget> list_widget =
    ListTile.divideTiles(context: context, tiles: list_tile).toList();

    return new ListView(children: list_widget);
  }
}
